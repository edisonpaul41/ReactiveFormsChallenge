# Live
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.1.5.
## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
## Build
Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.
## Running unit tests
Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).
## Architecture
**SRC Folder:**
 - It contains 3 important things, our app, assets and environment folders, here we only have stuff that is really general for the application, the base of the application, every part of our application should be able to access this folders and files.

**App Folder:** 
 - The main base code is here, and it is where the application will start on the app.component.ts, a pretty common wrong way of doing Angular is declaring services and components here, this can cause that the initial start of the application is overloaded with usefulness stuff that for the first load is unnecessary, only the Core Module is declared and the Lazy Loading of the other modules.

**Core Folder:**
 - Here we declare everything that have this 2 characteristics: Is being used in all the application and is needed an single instance of it across all the application, for example the authentication service.

**Modules Folder:**
 - As I mentioned before, we are using lazy loading of modules, it means that it will only load the module that is needed by the user, nothing else. Each module has it owns module.module.ts which has all the imports of the services and components to make itself works, also it has a module.routing.ts which is in charge of letting Angular know what CONTAINER load based on the URL.
    - **Layouts Folder**:
   This components are the mos general way of presenting a web page to an user, most of the time it only should have the navbar, a router outlet and a footer.
    - **Containers Folder**:
   When a route changes, the router outlet will be filled with this container and is responsible for managing all the logic behind the components inside it.
    - **Components Folder**:
   It only contains small parts of views that will be used by the container, its important to know that a component should only take care of itself and how it looks, it has to receive data and send data from the container to the container again, most of the time, it shouldn't import any services or have to much logic inside it, the container is responsible for that. 
## Features

 - Lazy Loading of modules.
 - Bootstrap 4.0 and Material design.
 - Last Angular Version.
 - Unit Testing.
 

## What it does?

-   ● The form will have a fake save button that will get disabled if:
    
    -   ○ One of the attributes name is duplicated among them.
        
    -   ○ One of the attribute component is invalid (more info below) Add tabs to the form
        
        and distribute the attributes in a given number of categories
        
-   ● Categories are static and only to allow the user to easily recognize the attributes.
    
-   ● Internally all the attributes belong to the same list but each one should have a property
    
    that allows the tab to render attributes, which belong to the tab’s category.
    
-   ● Have an add attribute button to dynamically add attributes in the current selected
    
    category.
    
-   ● Each attribute can be removed with a delete icon within the component.
    
-   ● The overall design is not required, use a front end framework to do it (preferably material
    
    design)
