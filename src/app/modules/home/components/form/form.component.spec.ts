import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from '../../../shared/shared.module';
import { FormComponent } from './form.component';
import { NumberFormComponent } from '../number-form/number-form.component';
import { NoneFormComponent } from '../none-form/none-form.component';
import { Data } from '../../../../models/data.model';
describe('Form Component', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,
                SharedModule
            ],
            declarations: [
                FormComponent,
                NumberFormComponent,
                NoneFormComponent
            ],
        }).compileComponents();
    }));
    it('should create the FormComponent', async(() => {
        const fixture = TestBed.createComponent(FormComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    }));
    it('should add a new enum to the dataModel', async(() => {
        const fixture = TestBed.createComponent(FormComponent);
        const component = fixture.componentInstance;
        component.data = new Data;
        component.addEnum('TEST');
        expect(component.data.enumerations.length).toEqual(1);
    }));
    it('should delete a enum from the dataModel', async(() => {
        const fixture = TestBed.createComponent(FormComponent);
        const component = fixture.componentInstance;
        component.data = new Data;
        component.addEnum('TEST');
        component.deleteEnum(0);
        expect(component.data.enumerations.length).toEqual(0);
    }));
});
