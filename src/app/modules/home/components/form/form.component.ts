import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Data } from '../../../../models/data.model';

@Component({
    selector: 'app-form-component',
    templateUrl: 'form.component.html',
    styleUrls: ['./form.component.scss']
})

export class FormComponent implements OnInit {
    @Input() data: Data;
    @Input() id: number;
    @Output() dataTypeEmitter = new EventEmitter;
    @Output() formatEmitter = new EventEmitter;
    @Output() deleteAttributeEmitter = new EventEmitter;
    dataTypes = ['STRING', 'OBJECT'];
    formats = ['NONE', 'NUMBER', 'BOOLEAN', 'DATE-TIME', 'CDATA', 'URI'];
    constructor() { }

    ngOnInit(): void {
        this.data.form.get('dataType').valueChanges.subscribe(value => {
            if (value) {
                this.dataTypeEmitter.emit({ event: value, id: this.id });
            }
        });
        this.data.form.get('format').valueChanges.subscribe(value => {
            if (value) {
                this.formatEmitter.emit({ event: value, id: this.id });
            }
        });
        if (this.data.form.get('dataType').value === null || this.data.form.get('dataType').value === '') {
            this.data.form.get('dataType').setValue('STRING');
            this.data.form.get('format').setValue('NONE');
        }
    }

    addEnum($event): void {
        if ($event !== '' && this.data.enumerations.filter(singleEnum => singleEnum === $event).length == 0) {
            this.data.enumerations.push($event);
        }
    }

    deleteEnum($event: number): void {
        this.data.enumerations.splice($event, 1);
    }

    deleteAtttribute(): void {
        this.deleteAttributeEmitter.emit(this.id);
    }
}