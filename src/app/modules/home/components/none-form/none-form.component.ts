import { Component, Input, Output, EventEmitter } from '@angular/core';

import { Data } from '../../../../models/data.model';

@Component({
    selector: 'app-none-form',
    templateUrl: 'none-form.component.html',
    styleUrls: ['./none-form.component.scss']
})

export class NoneFormComponent {
    @Input() data: Data = new Data;
    @Output() addEnumEmitter = new EventEmitter;
    @Output() deleteEnumEmitter = new EventEmitter;
    showRepeatedError = false;
    newEnum = '';

    addEnum(): void {
        if (this.data.enumerations.find(e => e === this.newEnum)) {
            this.showRepeatedError = true;
            return;
        }
        if (this.newEnum !== '') {
            this.showRepeatedError = false;
            this.addEnumEmitter.emit(this.newEnum);
            this.newEnum = '';
        }
    }

    deleteEnum(index: number): void {
        this.deleteEnumEmitter.emit(index);
    }
}