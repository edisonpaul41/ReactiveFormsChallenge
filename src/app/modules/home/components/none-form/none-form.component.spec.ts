import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from '../../../shared/shared.module';
import { NoneFormComponent } from './none-form.component';
describe('None Form Component', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,
                SharedModule
            ],
            declarations: [
                NoneFormComponent
            ],
        }).compileComponents();
    }));
    it('should create the NoneFormComponent', async(() => {
        const fixture = TestBed.createComponent(NoneFormComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    }));
    it('should not add enum to the listif it is an empty string', async(() => {
        const fixture = TestBed.createComponent(NoneFormComponent);
        const component = fixture.componentInstance;
        component.newEnum = '';
        component.addEnum();
        expect(component.newEnum).toEqual('');
    }));
});
