import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NumberFormComponent } from '../../components/number-form/number-form.component';
import { SharedModule } from '../../../shared/shared.module';
describe('Number Form Component', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,
                SharedModule
            ],
            declarations: [
                NumberFormComponent
            ],
        }).compileComponents();
    }));
    it('should create the NumberFormComponent', async(() => {
        const fixture = TestBed.createComponent(NumberFormComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    }));
});
