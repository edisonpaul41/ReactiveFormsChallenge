import { Component, Input } from '@angular/core';

import { Data } from '../../../../models/data.model';

@Component({
    selector: 'app-number-form-component',
    templateUrl: 'number-form.component.html',
})

export class NumberFormComponent {
    @Input() data: Data;
}