import { Component, Input, Output, EventEmitter } from '@angular/core';

import { DataModel } from '../../../../models/data.model';

@Component({
    selector: 'app-navbar-home-component',
    templateUrl: 'navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})

export class NavbarHomeComponent {
    @Input() dataModel: DataModel[];
    @Output() addTabEmitter = new EventEmitter;
    @Output() currentTabEmitter = new EventEmitter;
    newTabName: string = '';
    currentTab = '';

    addTab() {
        if (this.newTabName != '') {
            this.addTabEmitter.emit(this.newTabName);
            this.setCurrentTab(this.newTabName);
            this.newTabName = '';
        }
    }

    setCurrentTab($tabName) {
        this.currentTab = $tabName;
        this.currentTabEmitter.emit($tabName);
    }
}