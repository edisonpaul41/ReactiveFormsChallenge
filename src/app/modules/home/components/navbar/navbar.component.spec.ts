import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from '../../../shared/shared.module';
import { NavbarHomeComponent } from './navbar.component';
describe('Navbar Home Component', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,
                SharedModule
            ],
            declarations: [
                NavbarHomeComponent
            ],
        }).compileComponents();
    }));
    it('should create the NavbarHomeComponent', async(() => {
        const fixture = TestBed.createComponent(NavbarHomeComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    }));
    it('should add a new tab', async(() => {
        const fixture = TestBed.createComponent(NavbarHomeComponent);
        const component = fixture.componentInstance;
        component.newTabName = 'TEST';
        component.addTab();
        expect(component.newTabName).toEqual('');
    }));
    it('should set the current tab', async(() => {
        const fixture = TestBed.createComponent(NavbarHomeComponent);
        const component = fixture.componentInstance;
        component.newTabName = 'TEST';
        component.addTab();
        component.setCurrentTab('TEST');
        expect(component.currentTab).toEqual('TEST');
    }));
});
