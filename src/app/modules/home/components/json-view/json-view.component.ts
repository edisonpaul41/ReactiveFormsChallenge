import { Component, Input } from '@angular/core';

import { DataModel } from '../../../../models/data.model';

@Component({
    selector: 'app-json-view-component',
    templateUrl: 'json-view.component.html',
    styleUrls: ['./json-view.component.scss']
})

export class JsonViewComponent {
    @Input() dataModel: DataModel[];

    getText(): string {
        return this.beautyJSON(JSON.stringify(this.copyData(), null, 4));
    }

    beautyJSON(json: string): string {
        json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
        return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g,
            function (match) {
                let cls = 'number';
                if (/^"/.test(match)) {
                    if (/:$/.test(match)) {
                        cls = 'key';
                    } else {
                        cls = 'string';
                    }
                } else if (/true|false/.test(match)) {
                    cls = 'boolean';
                } else if (/null/.test(match)) {
                    cls = 'null';
                }
                return '<span class="' + cls + '">' + match + '</span>';
            });
    }

    copyData() {
        let data = {
            tabName: '',
            formData: [],
            enumerations: []
        };
        let dataArray = [];
        this.dataModel.map(single => {
            data.tabName = single.tabName;
            data.formData = single.data.map(form => form.form.getRawValue());
            data.enumerations = single.data.map(enumeration => enumeration.enumerations);
            dataArray.push(JSON.parse(JSON.stringify(data)));
        });
        return dataArray;
    }
}