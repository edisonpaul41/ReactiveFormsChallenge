import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HomeContainer } from './home.container';
import { NavbarHomeComponent } from '../../components/navbar/navbar.component';
import { FormComponent } from '../../components/form/form.component';
import { JsonViewComponent } from '../../components/json-view/json-view.component';
import { NoneFormComponent } from '../../components/none-form/none-form.component';
import { NumberFormComponent } from '../../components/number-form/number-form.component';
import { SharedModule } from '../../../shared/shared.module';
describe('Home Container', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,
                SharedModule
            ],
            declarations: [
                HomeContainer,
                NavbarHomeComponent,
                FormComponent,
                JsonViewComponent,
                NoneFormComponent,
                NumberFormComponent
            ],
        }).compileComponents();
    }));
    it('should create the HomeContainer', async(() => {
        const fixture = TestBed.createComponent(HomeContainer);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    }));
    it('should add a tab to the HomeContainer', async(() => {
        const fixture = TestBed.createComponent(HomeContainer);
        let component = fixture.componentInstance;
        component.addTab('TEST');
        expect(component.dataModel.length).toEqual(1);
    }));
    it('should set the current tab to the one created', async(() => {
        const fixture = TestBed.createComponent(HomeContainer);
        let component = fixture.componentInstance;
        component.addTab('TEST');
        expect(component.currentTab.tabName).toEqual('TEST');
    }));
    it('should add a new form to the current tab', async(() => {
        const fixture = TestBed.createComponent(HomeContainer);
        let component = fixture.componentInstance;
        component.addTab('TEST');
        component.addForm();
        expect(component.currentTab.data.length).toEqual(2);
    }));
    it('should return a new FORM', async(() => {
        const fixture = TestBed.createComponent(HomeContainer);
        let component = fixture.componentInstance;
        let form = component.getNewForm().form;
        expect(form.controls.name).not.toBeUndefined();
    }));
    it('should set the current tab to the choosed one', async(() => {
        const fixture = TestBed.createComponent(HomeContainer);
        let component = fixture.componentInstance;
        component.addTab('TEST');
        component.addTab('TEST2');
        component.setCurrentTab('TEST');
        expect(component.currentTab.tabName).toEqual('TEST');
    }));
    it('should delete an attribute from a tab', async(() => {
        const fixture = TestBed.createComponent(HomeContainer);
        let component = fixture.componentInstance;
        component.addTab('TEST');
        component.deleteAttribute(0);
        expect(component.currentTab.data.length).toEqual(0);
    }));
});
