import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl } from '@angular/forms';

import { DataModel, Data } from '../../../../models/data.model';

@Component({
    selector: 'app-home-container',
    templateUrl: 'home.container.html',
    styleUrls: ['./home.container.scss']
})

export class HomeContainer implements OnInit {
    dataModel: DataModel[] = [];
    currentTab: DataModel;
    dummyInitData = ['Device Info', 'Sensors', 'Settings', 'Commands', 'Metadata'];
    constructor(private formBuilder: FormBuilder) {
    }

    ngOnInit(): void {
        this.dummyInitData.map(e => {
            this.addTab(e);
        });
    }

    addTab($tabName: string): void {
        if (this.dataModel.find(e => e.tabName === $tabName) != undefined) {
            return;
        }
        const newDataModel = new DataModel;
        newDataModel.tabName = $tabName;
        newDataModel.data.push(this.getNewForm());
        this.dataModel.push(newDataModel);
        if (this.currentTab === undefined) {
            this.currentTab = this.dataModel[0];
        }
    }

    addForm(): void {
        this.currentTab.data.push(this.getNewForm());
    }

    getNewForm(): Data {
        const data = new Data;
        data.form = this.formBuilder.group({
            name: ['', [Validators.required, this.checkIfAttributeNameValid(this)]],
            description: ['', Validators.required],
            resourceType: [''],
            defaultValue: ['', Validators.required],
            dataType: ['', [Validators.required]],
            format: ['', [Validators.required]],
            number: this.formBuilder.group({
                rangeMax: ['', [Validators.required, this.onlyNumberValidator, this.checkIfMinAndMaxNumber]],
                rangeMin: ['', [Validators.required, this.onlyNumberValidator]],
                units: ['', [Validators.required]],
                precision: ['', [Validators.required, this.onlyNumberValidator, this.checkIfPrecisionAndAccuracy]],
                accuracy: ['', [Validators.required, this.onlyNumberValidator, this.checkIfPrecisionAndAccuracy]],
            })
        });
        data.form.get('resourceType').disable();
        return data;
    }

    setCurrentTab($tabName): void {
        this.currentTab = this.dataModel.find(data => data.tabName === $tabName);
    }

    setDataType($event): void {
        if ($event.event === 'OBJECT') {
            this.currentTab.data[$event.id].form.get('format').setValue(null);
            this.currentTab.data[$event.id].form.get('format').disable();
            this.currentTab.data[$event.id].form.get('defaultValue').disable();
            this.clearNumberValidators($event.id);
        } else {
            this.currentTab.data[$event.id].form.get('format').enable();
            this.currentTab.data[$event.id].form.get('defaultValue').enable();
        }
        this.currentTab.data[$event.id].enumerations = [];

    }

    setFormat($event): void {
        if ($event.event === 'NONE') {
            this.clearNumberValidators($event.id);
        } else if ($event.event === 'NUMBER') {
            this.addNumberValidators($event.id);
        } else {
            this.clearNumberValidators($event.id);
        }
        this.currentTab.data[$event.id].enumerations = [];
    }

    deleteAttribute(index: number): void {
        this.currentTab.data.splice(index, 1);
    }

    addNumberValidators(index: number): void {
        Object.values(this.currentTab.data[index].form.get('number')['controls']).forEach((control: AbstractControl) => {
            control.setValidators([Validators.required, this.checkIfMinAndMaxNumber]);
            control.updateValueAndValidity();
        });
        this.currentTab.data[index].form.get('number').get('accuracy').setValidators([this.checkIfPrecisionAndAccuracy]);
        this.currentTab.data[index].form.get('number').get('accuracy').updateValueAndValidity();
        this.currentTab.data[index].form.get('number').get('precision').setValidators([this.checkIfPrecisionAndAccuracy]);
        this.currentTab.data[index].form.get('number').get('precision').updateValueAndValidity();

    }

    clearNumberValidators(index: number): void {
        Object.values(this.currentTab.data[index].form.get('number')['controls']).forEach((control: AbstractControl) => {
            control.clearValidators();
            control.setValue(null);
            control.updateValueAndValidity();
        });
    }

    displayIfAnyErrorOnForm(form: FormGroup): void {
        Object.values(form.controls).forEach(control => {
            control.markAsTouched();
            control.markAsDirty();
        });
        Object.values(form.get('number')['controls']).forEach((control: AbstractControl) => {
            control.markAsTouched();
            control.markAsDirty();
        });
    }

    isTabAttributesValid(): boolean {
        return !(
            this.dataModel.filter(data => data.data.filter(form => form.form.valid === false).length > 0).length > 0
            );
    }

    onlyNumberValidator(val): any {
        if (val.value.match(/.*[^0-9].*/)) {
            return { inval: true };
        }
    }

    checkIfAttributeNameValid(context): any {
        const here = this;
        return function (val): any {
            if (here.currentTab !== undefined) {
                if ((!(here.dataModel.filter(data => data.data.filter(
                    form => form.form.get('name').value === val.value).length > 0).length <= 1)
                || !(here.currentTab.data.filter(form => form.form.get('name').value === val.value).length <= 1))) {
                    return { inval: true };
                }
            }
            return null;
        };
    }

    checkIfMinAndMaxNumber(val: FormControl): any {
        if (val.parent) {
            if (parseInt(val.parent.get('rangeMax').value) <= parseInt(val.parent.get('rangeMin').value)) {
                return { inval: true };
            }
        }
        return null;
    }

    checkIfPrecisionAndAccuracy(val: FormControl): any {
        if (val.parent) {
            if (!((parseInt(val.parent.get('rangeMax').value) - parseInt(val.parent.get('rangeMin').value)) % parseInt(val.value) === 0)) {
                return { inval: true };
            }
        }
        return null;
    }
}