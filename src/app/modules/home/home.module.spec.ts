import { HomeModule } from './home.module';

describe('HomeModule', () => {
    let homeModule: HomeModule;

    beforeEach(() => {
        homeModule = new HomeModule();
    });

    it('should create an instance of the Home Module', () => {
        expect(homeModule).toBeTruthy();
    });
});
