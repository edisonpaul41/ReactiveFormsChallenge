import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { SharedModule } from '../shared/shared.module';

import { HomeLayout } from './layouts/home/home.layout';

import { HomeContainer } from './containers/home/home.container';

import { NavbarHomeComponent } from './components/navbar/navbar.component';
import { FormComponent } from './components/form/form.component';
import { JsonViewComponent } from './components/json-view/json-view.component';
import { NoneFormComponent } from './components/none-form/none-form.component';
import { NumberFormComponent } from './components/number-form/number-form.component';

@NgModule({
  imports: [
    CommonModule,
    HomeRoutingModule,
    SharedModule
  ],
  declarations: [
    HomeContainer,
    HomeLayout,
    NavbarHomeComponent,
    FormComponent,
    JsonViewComponent,
    NoneFormComponent,
    NumberFormComponent
  ]
})
export class HomeModule { }
