import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatSidenavModule } from '@angular/material/sidenav';

import { NavbarComponent } from './components/navbar/navbar.component';
import { SelectInputComponent } from './components/select-input/select-input.component';
import { TextInputComponent } from './components/text-input/text-input.component';


@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        ReactiveFormsModule,
        FormsModule,
        MatSidenavModule
    ],
    exports: [
        NavbarComponent,
        SelectInputComponent,
        TextInputComponent,
        ReactiveFormsModule,
        FormsModule
    ],
    declarations: [
        NavbarComponent,
        SelectInputComponent,
        TextInputComponent
    ],
    providers: [],
})
export class SharedModule { }
