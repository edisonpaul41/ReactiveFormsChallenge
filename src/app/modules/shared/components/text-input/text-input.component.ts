import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
    selector: 'app-text-input',
    templateUrl: 'text-input.component.html',
    styleUrls: ['./text-input.component.scss']
})

export class TextInputComponent {
    @Input() label: string;
    @Input() dataForm: FormGroup;
    @Input() controlName: string;
    @Input() onlyNumber: boolean = false;
    @Input() customError: string = '';
    verifyOnlyNumber() {
        if (this.onlyNumber) {
            this.dataForm.get(this.controlName)
                .setValue(this.dataForm.get(this.controlName).value.replace(/\D/g, ''));
        }
    }

    getIfError() {
        return  this.dataForm.get(this.controlName).touched &&
            this.dataForm.get(this.controlName).dirty &&
            this.dataForm.get(this.controlName).invalid;
    }
}