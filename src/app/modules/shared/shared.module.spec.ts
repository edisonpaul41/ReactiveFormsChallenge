import { SharedModule } from './shared.module';

describe('SharedModule', () => {
    let homeModule: SharedModule;

    beforeEach(() => {
        homeModule = new SharedModule();
    });

    it('should create an instance of the Shared Module', () => {
        expect(homeModule).toBeTruthy();
    });
});
