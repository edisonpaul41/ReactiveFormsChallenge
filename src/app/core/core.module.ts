import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
    imports: [
        BrowserAnimationsModule
    ],
    exports: [
        BrowserAnimationsModule
    ],
    declarations: [],
    providers: [],
})
export class CoreModule { }
