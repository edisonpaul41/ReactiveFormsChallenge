import { FormGroup } from '@angular/forms';

export class DataModel {
    data: Data[] = [];
    tabName: string;
}
export class Data {
    form: FormGroup;
    enumerations: string[] = [];
}